#-------------------------------------------------
#
# Project created by QtCreator 2013-10-23T13:28:37
#
#-------------------------------------------------

QT       += core gui

QT       +=webkitwidgets

QT       += serialport

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets


TARGET    = boat_pc
TEMPLATE  = app


SOURCES  += src/main.cpp\
            src/mainwindow.cpp \
            src/mythread.cpp \
            src/mywaterinf.cpp \
    	    src/mymap.cpp \


HEADERS  += include/mainwindow.h \
            include/mythread.h \
    	    include/mymap.h \
            include/mywaterinf.h

FORMS    += mainwindow.ui

RC_ICONS  = image/ab.ico
