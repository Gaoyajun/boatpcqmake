#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QMessageBox>
#include <QDateTime>
#include <QString>
#include <QtCore/QDebug>
#include <QFile>
#include <QVariant>
#include<QTime>

#include <QtSerialPort/QSerialPort>
#include <QtSerialPort/QSerialPortInfo>
#include <ui_mainwindow.h>

#include<qmath.h>

#include "include/mythread.h"
#include "include/mymap.h"
#include "include/mywaterinf.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();
    void ui_init(void);
    void control();
    MyThread threadA;
    Mymap* myMap;
    MyWaterInf *myWaterInf;

    QString targetLat;
    QString targetLon;

signals:
    void sendWaterInf(const QString  &);
    void sendCurrentLoc(const QString &,const QString &);
    void sendBoatState(bool);

public slots:
    void getTargetLoc(const QString & , const QString &);

private slots:
    void displayRxData();

    void on_pushButtonOpenSerial_clicked();

    void on_pushButtonCloseSerial_clicked();

    void on_pushButtonClearDisplay_clicked();

    void on_pushButtonExit_clicked();

    void on_pushButtonSendSpeed_clicked();

    void on_radioButtonLeft_clicked();

    void on_radioButtonRight_clicked();

    void on_radioButtonStraight_clicked();

    void on_radioButtonStop_clicked();

    void on_radioButtonManualControl_clicked();

    void on_radioButtonNavigation_clicked();

    void on_pushButtonStart_pressed();

    void on_pushButtonBaiduMap_clicked();

    void on_pushButtonCloseSystem_clicked();

    void on_pushButtonWaterInf_clicked();

private:
    Ui::MainWindow *ui;

};

#endif // MAINWINDOW_H
