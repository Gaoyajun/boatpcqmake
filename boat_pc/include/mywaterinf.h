#ifndef WATERINFCLASS
#define WATERINFCLASS

#include <QLabel>
#include <QDialog>
#include <QLineEdit>
#include <QGridLayout>
#include <QPushButton>

class MyWaterInf: public QDialog
{
    Q_OBJECT
public:
    MyWaterInf();

    QLineEdit       *lineEditWaterInf;
    QLabel            *laberWaterInf;
    QPushButton *pushbuttonQuit;

private:

signals:

public slots:
    void getWaterInf(const QString &);         //Send按钮单击事件连接的槽函数，目的为发射infoSend信号
};
#endif // WATERINFCLASS

