#ifndef MYMAP_H
#define MYMAP_H

#include <QLineEdit>
#include <QLabel>
#include <QPushButton>
#include <QGridLayout>
#include <QWebFrame>
#include <QWebView>
#include <QMessageBox>
#include <QDialog>

class Mymap : public QDialog
{
    Q_OBJECT
public:
    Mymap();

    QWebView *baiduMap;

    QLineEdit *LineEditTargetLat;
    QLineEdit *LineEditTargetLon;
    QLineEdit *LineEditCurrentLat;
    QLineEdit *LineEditCurrentLon;
    QLineEdit *LineEditIdentification;

    QLabel *LabelTargetLat;
    QLabel *LabelTargetLon;
    QLabel *LaberCurrentLat;
    QLabel *LaberCurrentLon;

    QPushButton *pushbuttonSetIdentification;
    QPushButton *pushbuttonSendTargetLocal;
    QPushButton *pushbuttonQuit;
    QPushButton *pushbuttonGetDistance;
private:
    int radiusSize_;
    QString strCity_;

signals:
    void sendTargetLoc(const QString & , const QString &); //定义消息发送信号
public slots:
    void on_webView_loadFinished(bool);
    void on_map_button_size_clicked();
    void on_map_Button_clicked();
    //void boat_loc();    //实时的定位船的当前位置和目标位置
    void updateLocal();
    void on_getDistance_clicked();

    void getBoatState(bool);

    void emitSignalSendTarget();      //Send按钮单击事件连接的槽函数，目的为发射sendTarget信号

    void getCurrentLoc(const QString & , const QString &);
};

#endif // MAPCLASS_H

