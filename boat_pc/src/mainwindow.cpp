#include "include/mainwindow.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    this->ui_init();

    myWaterInf = new MyWaterInf;
    myMap        = new  Mymap;

    ui->pushButtonCloseSerial->setEnabled(false);
    ui->pushButtonStart->setEnabled(false);
    ui->pushButtonCloseSystem->setEnabled(false);
    ui->pushButtonWaterInf->setEnabled(false);

    this->threadA.changeTxState(false);
    this->threadA.TxData.clear();
    connect(&this->threadA, SIGNAL(comRecive()), this, SLOT(displayRxData()));
    connect(this,SIGNAL(sendWaterInf(const QString &)),this->myWaterInf,SLOT( getWaterInf(const QString &)));
    connect(this,SIGNAL(sendCurrentLoc(const QString & , const QString & )) , this->myMap , SLOT(getCurrentLoc(const QString &, const QString &)));
    connect(this,SIGNAL(sendBoatState(bool)) , this->myMap , SLOT(getBoatState(bool)));
    connect(this, SIGNAL(sendCurrentLoc( const QString &, const QString &)) , &this->threadA, SLOT(getCurrentLoc( const QString & , const QString &)));
    connect(this->myMap,SIGNAL(sendTargetLoc(const QString &, const QString &)) , &this->threadA , SLOT(getTargetLoc(const QString &, const QString &)));
    connect(this->myMap,SIGNAL(sendTargetLoc(const QString &, const QString &)) , this , SLOT(getTargetLoc(const QString &, const QString &)));
    connect(&this->threadA,SIGNAL(updateLocalSignal()),this->myMap,SLOT(updateLocal()));
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::ui_init(void)
{
    foreach (const QSerialPortInfo &info, QSerialPortInfo::availablePorts())
    {
        qDebug() << "Name        : " << info.portName();
        qDebug() << "Description : " << info.description();
        qDebug() << "Manufacturer: " << info.manufacturer();

        // Use QSerialPort
        QSerialPort serial;
        serial.setPort(info);
        if (serial.open(QIODevice::ReadWrite))
        {
           ui->comboBoxSerialNum->addItem(info.portName());
            serial.close();
        }
    }
}

void MainWindow::on_pushButtonOpenSerial_clicked()
{
    if(ui->comboBoxSerialNum->currentIndex()!=0)
    {
        if(ui->comboBoxBaudRate->currentIndex()!=0)
        {
            threadA.startCom();                      //打开串口
            qDebug() << "Brush:" <<"thread starting";
            threadA.setPortnum(ui->comboBoxSerialNum->currentText());
            threadA.changeComState(false);          //com_opened
            ui->pushButtonOpenSerial->setText("串口已打开");
            ui->textEditSystemState->append("串口打开完成");

            ui->pushButtonCloseSerial->setEnabled(true);
            ui->pushButtonWaterInf->setEnabled(true);
            ui->pushButtonStart->setEnabled(true);
            ui->pushButtonOpenSerial->setEnabled(false);
        }
        else
             QMessageBox::information(this,tr("Tips"),tr("请选择波特率"),QMessageBox::Yes);
    }
    else
        QMessageBox::information(this,tr("Tips"),tr("请选择串口号"),QMessageBox::Yes);
}

void MainWindow::on_pushButtonCloseSerial_clicked()
{
    bool systemState=ui->pushButtonCloseSystem->isEnabled();
    if(systemState==false)
    {
        qDebug() << "Brush:" <<"thread1 stoping";
        threadA.stop();                              //关闭串口
        qDebug() << "Brush:" <<"com close";
        ui->textEditSystemState->append("串口关闭完成");
        ui->pushButtonOpenSerial->setText("打开串口");
        ui->pushButtonCloseSerial->setEnabled(false);
        ui->pushButtonOpenSerial->setEnabled(true);
    }
    else
        QMessageBox::information(this,"Tips","Please close the system!",QMessageBox::Yes);
}

//清空接收区域和输出区域
void MainWindow::on_pushButtonClearDisplay_clicked()
{
    ui->textEditSystemState->clear();
    ui->textEditReceiveInf->clear();
}

void MainWindow::on_pushButtonExit_clicked()
{
        QString stopCommand="$P#";
        threadA.TxData=((const char*)stopCommand.toLocal8Bit());
        threadA.changeTxState(true);
        QTime dieTime = QTime::currentTime().addMSecs(50);
        while( QTime::currentTime() < dieTime )
        QCoreApplication::processEvents(QEventLoop::AllEvents, 50);

        qDebug() << "Brush:" <<"thread1 stoping";
        threadA.stop();
        threadA.quit();
        threadA.exit(0);
        threadA.wait();
        qDebug() << "Brush:" <<"thread1 stoped";
}

void MainWindow::displayRxData()
{
    QString str,currentLat="0.0",currentLon="0.0",headAngle,waterInf="0.0";

    char tmp[100];
    char *buf;
    char var;
    emit(this->sendWaterInf(waterInf));

    QDateTime *datatime=new QDateTime(QDateTime::currentDateTime());   //获取当前时间
    if(threadA.requestData.size()>0)
   {
        int c_pos=0,d_pos=0;
        char data_b;
        bool dataSymbal=0,dataCheck=0;
        str="收到数据: ";
        str+=datatime->time().toString();
        ui->textEditReceiveInf->append(str);
        str.clear();

        buf=threadA.requestData.data();
        //information form is $,2212.3456,13123.4567,45.324,-66.8#
        for(var=0;var<threadA.requestData.size();var++)
        {
            ::snprintf(tmp,100, "%02X", (unsigned char)(*buf));
            if(*buf=='$'||dataSymbal==1)
            {
                data_b=*buf;
                if(data_b=='F')
                {
                    ui->textEditSystemState->append("控制信息发送失败，请重新发送！");
                }
                if(data_b=='S')
                {
                    ui->textEditSystemState->append("控制信息发送成功！");
                }
                if(data_b=='#')       //验证后面是否丢失数据
                {
                    dataCheck=1;
                    break;
                }
                if(data_b==',')
                {
                    c_pos++;
                    d_pos=0;
                }
                else
                {
                    switch(c_pos)
                    {
                    case 1:if(d_pos<9)
                            currentLat[d_pos++]=data_b;
                        break;
                    case 2:if(d_pos<10)
                            currentLon[d_pos++]=data_b;
                        break;
                    case 3:if(d_pos<6)
                            headAngle[5-d_pos]=data_b;
                            d_pos++;
                        break;
                     case 4 : if(d_pos<5)
                            waterInf[d_pos++]=data_b;
                        break;
                    default:
                        break;
                    }
                }
                dataSymbal=1;
            }
            buf++;

            ui->lineEditBoatHead->setText(headAngle);

            emit(this->sendWaterInf(waterInf));
        }

        //判断选择的是那种模式，如果都没选择，船不动
        if(ui->radioButtonNavigation->isChecked())
            threadA.boat_mode_N();
        else if(ui->radioButtonManualControl->isChecked())
            threadA.boat_mode_P();
        else{
            threadA.TxData="$P#";
            threadA.changeTxState(true);
        }

        //进一步确定接收到的经纬度是否正确
        int temp;
        double doubleCurrentLon=currentLon.toDouble();
        double doubleCurrentLat=currentLat.toDouble();
        temp=qFloor( doubleCurrentLat/100 ) ;    //取小于这个浮点数的最大整数,
        doubleCurrentLat=temp+(doubleCurrentLat-temp*100)/60;
        temp=qFloor( doubleCurrentLon/100 ) ;    //取小于这个浮点数的最大整数,
        doubleCurrentLon=temp+(doubleCurrentLon-temp*100)/60;

        doubleCurrentLon += 0.011505;
        doubleCurrentLat  += 0.002922;

        currentLon=QString::number(doubleCurrentLon,'g',9);
        currentLat=QString::number(doubleCurrentLat,'g',8);

        //Have to sure the first value is true!!!
        static QString preLat =currentLat;
        static QString preLon=currentLon;

//        bool checkLat = fabs(currentLat.toDouble()-preLat.toDouble())<1;
//        bool checkLon =fabs(currentLon.toDouble()-preLon.toDouble())<1;

//        if(checkLat && checkLon && dataCheck==1)
//        {
//            preLat = currentLat;
//            preLon =currentLon;

            ui->lineEditCurrentLon->setText(currentLon);     //显示目前的经纬度
            ui->lineEditCurrentLat->setText(currentLat);
            emit(sendCurrentLoc(currentLat,currentLon));
//        }
      }
    bool boatState=ui->radioButtonNavigation->isChecked();
    if(boatState==true){
        emit(sendBoatState(true));
    }
    else
        emit(sendBoatState(false));

    threadA.requestData.clear();
}

void MainWindow::on_pushButtonSendSpeed_clicked()
{
    bool systemState=ui->pushButtonCloseSystem->isEnabled();
    if(systemState==true)
    {
        bool isMaunalControl= ui->radioButtonManualControl->isChecked();
        if(isMaunalControl)
        {
            bool isStraight=ui->radioButtonStraight->isChecked();
            if(isStraight==true)
            {
                float targetSpeed;
                QString targetSpeedString=ui->lineEditTargetSpeed->text();
                targetSpeed=targetSpeedString.toFloat();
                if(targetSpeed>10||targetSpeed<0)
                {
                    QMessageBox::information(this,"Tips","输入信息不正确，速度值为0~10km/h",QMessageBox::Yes);
                }
                else
                {
                    if(targetSpeed==0)
                        targetSpeed=0;
                    else  if(targetSpeed>0&&targetSpeed<1)
                        targetSpeed=targetSpeed*25+30;
                    else
                        targetSpeed=targetSpeed*25+30/targetSpeed;
                    int targetSpeetInt=qFloor( targetSpeed );      //将控制量取整    因为船在给的pwm值小于35时不转
                    targetSpeedString =QString::number(targetSpeetInt,10);  //将控制量转化成QString类型,准备发送出去
                    if(targetSpeetInt==0)
                    {
                        targetSpeedString="$,00"+targetSpeedString+",00"+targetSpeedString+"#";
                    }
                    else if(targetSpeetInt<100)
                    {
                        targetSpeedString="$,0"+targetSpeedString+",0"+targetSpeedString+"#";
                    }
                    else
                    {
                        targetSpeedString="$,"+targetSpeedString+","+targetSpeedString+"#";
                    }
                    qDebug()<<targetSpeedString;
                    threadA.TxData=((const char*)targetSpeedString.toLocal8Bit());
                    threadA.changeTxState(true);
                }
            }
            else
                QMessageBox::information(this,"Tips","请选择直线前进模式",QMessageBox::Yes);
        }
        else
            QMessageBox::information(this,"Tips","请选择远程控制模式",QMessageBox::Yes);
    }
    else
        QMessageBox::information(this,"Tips","请开启系统",QMessageBox::Yes);
}

void MainWindow::on_radioButtonLeft_clicked()  //bool checked
{
    bool systemState=ui->pushButtonCloseSystem->isEnabled();
    if(systemState==true)
    {
        bool isMaunalControl=ui->radioButtonManualControl->isChecked();
        if(isMaunalControl==true)
        {
            threadA.TxData="$L#";
            threadA.changeTxState(true);
        }
        else
            QMessageBox::information(this,"Tips","请选择远程控制模式",QMessageBox::Yes);
    }
    else
        QMessageBox::information(this,"Tips","请开启系统",QMessageBox::Yes);
}

void MainWindow::on_radioButtonRight_clicked()
{
    bool systemState=ui->pushButtonCloseSystem->isEnabled();
    if(systemState==true)
    {
        bool isMaunalControl=ui->radioButtonManualControl->isChecked();
        if(isMaunalControl)
        {
            threadA.TxData="$R#";
            threadA.changeTxState(true);
        }
        else
            QMessageBox::information(this,"Tips","请选择远程控制模式",QMessageBox::Yes);
    }
    else
        QMessageBox::information(this,"Tips","请开启系统",QMessageBox::Yes);
}

void MainWindow::on_radioButtonStraight_clicked()
{
    bool systemState=ui->pushButtonCloseSystem->isEnabled();
    if(systemState==true)
    {
        bool isMaunalControl=ui->radioButtonManualControl->isChecked();
        if(isMaunalControl)
        {
            threadA.TxData="$S#";
            threadA.changeTxState(true);
        }
        else
            QMessageBox::information(this,"Tips","请选择远程控制模式",QMessageBox::Yes);
    }
    else
        QMessageBox::information(this,"Tips","请开启系统",QMessageBox::Yes);
}

void MainWindow::on_radioButtonStop_clicked()
{
    bool systemState=ui->pushButtonCloseSystem->isEnabled();
    if(systemState==true)
    {
        bool isMaunalControl=ui->radioButtonManualControl->isChecked();
        if(isMaunalControl)
        {
            threadA.TxData="$P#";
            threadA.changeTxState(true);
        }
        else
             QMessageBox::information(this,"Tips","请选择远程控制模式",QMessageBox::Yes);
    }
    else
        QMessageBox::information(this,"Tips","请开启系统",QMessageBox::Yes);
}

void MainWindow::on_radioButtonManualControl_clicked()
{
    bool isMaunalControl=ui->radioButtonManualControl->isChecked();
    if(isMaunalControl)
    {
        threadA.boat_mode_P();       //关闭自动导航模式
    }
}

void MainWindow::on_radioButtonNavigation_clicked()
{
    bool isNavigation=ui->radioButtonNavigation->isChecked();
    if(isNavigation)
    {
        threadA.TxData="$S#";      //进入自主导航后，就设置直线前进
        threadA.changeTxState(true);
        threadA.boat_mode_N();      //开启自动导航模式
    }
}

void MainWindow::on_pushButtonStart_pressed()
{
    bool isManualControl = ui->radioButtonManualControl->isChecked();
    bool isNavigation = ui->radioButtonNavigation->isChecked();
    if(isManualControl || isNavigation)
    {
        threadA.startCom();                      //打开串口   打开系统的时候，也打开船头准备接收数据
        qDebug() << "Brush:" <<"thread starting";
        threadA.setPortnum(ui->comboBoxSerialNum->currentText());
        threadA.setPortBaudRate(ui->comboBoxBaudRate->currentText());
        threadA.changeComState(false);
        threadA.start();

        QTime dieTime = QTime::currentTime().addMSecs(20);
        while( QTime::currentTime() < dieTime )
            QCoreApplication::processEvents(QEventLoop::AllEvents, 20);
        threadA.boat_system_start_n();                 //开启系统，约束自主导航的
        ui->textEditSystemState->append("系统已开启");
        ui->pushButtonStart->setText("系统开启");

        ui->pushButtonStart->setEnabled(false);
        ui->pushButtonCloseSystem->setEnabled(true);
    }
    else
        QMessageBox::information(this,"Tips","请选择控制模式!",QMessageBox::Yes);
}

void MainWindow::on_pushButtonBaiduMap_clicked()
{
    myMap->show();
}

void MainWindow::on_pushButtonCloseSystem_clicked()
{
    bool closeSystem=ui->pushButtonCloseSystem->isEnabled();
    if(closeSystem==true)
    {
        QString StopCommand="$P#";
        threadA.TxData=((const char*)StopCommand.toLocal8Bit());
        threadA.changeTxState(true);

        threadA.boat_system_stop_n();

        ui->textEditSystemState->append("系统已关闭");
        ui->pushButtonStart->setText("系统关闭");
        QTime dieTime = QTime::currentTime().addMSecs(20);
        while( QTime::currentTime() < dieTime )
        QCoreApplication::processEvents(QEventLoop::AllEvents, 20);
        threadA.stop();

        ui->pushButtonStart->setEnabled(true);
        ui->pushButtonCloseSystem->setEnabled(false);
    }
}

void MainWindow::on_pushButtonWaterInf_clicked()
{
    myWaterInf->show();
}

void MainWindow::getTargetLoc(const QString &lat, const QString &lon)
{
    targetLat  = lat;
    targetLon =lon;
    ui->lineEditTargetLat->setText(targetLat);
    ui->lineEditTargetLon->setText(targetLon);
}
