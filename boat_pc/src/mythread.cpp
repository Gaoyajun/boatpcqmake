#include "include/mythread.h"

using namespace std;

void controlNavigation(QString s_long,QString s_lat,QString c_long,QString c_lat);//控制下位机
double getDistance(double lat1,double lon1,double lat2,double lon2);//测量目标位置和目前位置的距离
double getHeading(double lat1,double lon1,double lat2,double lon2);

const double ARC2ANGLE = 57.2957795;
const double ANGLE2ARC = 0.0174532925;
const int THRESHOLD = 20;

QString motLeftValue, motRightValue;//调节电机的PWM来到达目标地点

MyThread::MyThread()
{
    requestData.resize(20);
    stopped_=false;
}

void MyThread::run()
{
    int countTimes=0;
    QSerialPort *mySerialPort= new QSerialPort;
    while(!stopped_)
    {
        if(stopped_&&com_opened_)
        {
            mySerialPort->close();
            com_opened_=false;
        }
        if(!com_opened_)
        {
            //open com
            qDebug() << "Brush:" <<"---open com port------";
            com_opened_=true;
            mySerialPort->setPortName(portnum_);
            mySerialPort->open(QIODevice::ReadWrite);
            mySerialPort->setBaudRate(portBautRate_.toInt());
            mySerialPort->setDataBits(QSerialPort::Data8);
            mySerialPort->setParity(QSerialPort::NoParity);
            mySerialPort->setStopBits(QSerialPort::OneStop);
            mySerialPort->setFlowControl(QSerialPort::NoFlowControl);
            com_opened_=true;
        }
        if(this->com_opened_&&this->sendEvent_)
        {
              this->sendEvent_=false;
              mySerialPort->clear(QSerialPort::AllDirections);
              mySerialPort->write(TxData);
              if (mySerialPort->waitForBytesWritten(5))
              {
                qDebug() << "Brush:" <<"send data success";
                qDebug()<<QDateTime::currentDateTime().toString();
                if (mySerialPort->waitForReadyRead(1500))  //1s
                {
                    requestData = mySerialPort->readAll();
                    while (mySerialPort->waitForReadyRead(15))
                    requestData += mySerialPort->readAll();
                    emit(this->comRecive());
                }
                else
                {
                        qDebug() << "Brush:" <<"wait return time out";
                }
              }
              else
              {
                  qDebug() << "Brush:" <<"send time out";
              }
        }
    if (mySerialPort->waitForReadyRead(5))  //50ms
    {
        while (mySerialPort->waitForReadyRead(5))
        this->msleep(20);
        requestData = mySerialPort->readAll();
        emit(this->comRecive());     //发出信号，准备显示接收到的数据
    }
    if(stopped_&&com_opened_)
    {
        mySerialPort->close();
        com_opened_=false;
    }
    countTimes++;
    if(countTimes%400==0)
    {
        qDebug()<<"update map";
        emit(this->updateLocalSignal());     //实时的更新船的位置
        countTimes=0;
        if(boatMode_)             //首先确保系统已开启，然后再确保系统进入自主导航模式
        {
            controlNavigation( currentLon, currentLat, targetLon, targetLat);
            qDebug()<<"currentLon="<<currentLon<<"   currentLat="<<currentLat<<"    targetLon="<<targetLon<<"     targetLat="<<targetLat;
            QString pwm1="$,"+motLeftValue+motRightValue+"#";
            qDebug()<<"Rx message is:"<<pwm1;
            TxData=(const char*)pwm1.toLocal8Bit();
            changeTxState(true);
        }
       }
    }
}

void controlNavigation(QString currentLonString,QString currentLatString,QString targetLonString,QString targetLatString)
{
    double currentLat=0,currentLon=0,targetLat=0,targetLon=0;
    currentLat=currentLatString.toDouble();
    currentLon=currentLonString.toDouble();
    targetLat=targetLatString.toDouble();
    targetLon=targetLonString.toDouble();

    //记录起始点的位置信息
    static double preLat=currentLat;
    static double preLong=currentLon;

    double distance=getDistance(currentLat,currentLon,targetLat,targetLon);  //测目标位置和目前位置的距离

    qDebug()<<"Distance is:"<<distance;
    qDebug()<<"Target:\t"<<targetLonString<<' '<<targetLatString;
    qDebug()<<"Current:\t"<<currentLonString<<' '<<currentLatString;
    qDebug()<<"Pre:\t"<<QString::number(preLong,'g',9)<<' '<<QString::number(preLat,'g',8);

    if(distance>3)    //设置一个阈值，确定若到达目标地点的一个范围就认为已经到达目标地点，此处暂时设为3米
    {
        double diffLat = currentLat-preLat;
        double diffLon = currentLon-preLong;

        qDebug()<<"After:diff:\t"<<QString::number(diffLon,'g',9)<<' '<<QString::number(diffLat,'g',8);
        if(fabs(diffLat)<0.000001 && fabs(diffLon)<0.000001){
            qDebug()<<"Didn't move!";
            motLeftValue="050";
            motRightValue=",050";
            return;
        }
        double angleOfTarget = atan2(targetLat-currentLat,targetLon-currentLon);
        double angleOfCurrent = atan2(diffLat,diffLon);
        double angle = angleOfTarget-angleOfCurrent;

        angleOfTarget *= ARC2ANGLE;
        angleOfCurrent *= ARC2ANGLE;
        angle = angle * ARC2ANGLE;
        qDebug()<<"AngleOfTarget:\t"<<angleOfTarget;
        qDebug()<<"AngleOfCurrent:\t"<<angleOfCurrent;
        qDebug()<<"Angle:\t\t"<<angle;
        if ( (angle<-THRESHOLD && angle>THRESHOLD-180) || (angle>THRESHOLD+180 && angle<360-THRESHOLD) ){
            motLeftValue="050";
            motRightValue=",100";
        }
        else if( (angle>THRESHOLD && angle<180-THRESHOLD) || (angle<-180-THRESHOLD && angle>THRESHOLD-360) ){
            motLeftValue="100";
            motRightValue=",050";
        }
        else{
            motLeftValue="050";
            motRightValue=",050";
        }
        preLat=currentLat;
        preLong=currentLon;
    }
    //距离小于三米就认为船已经到达目标位置
    else{
        motLeftValue  ="000";
        motRightValue=",000";
    }
}

double getDistance(double lat1,double lon1,double lat2,double lon2){
    double distance;
    double mLat = (lat2 - lat1)*110946.0;
    double mLon = (lon2 - lon1)* cos(((lat2 + lat1)/2)* 0.0174532925)*111318.0 ;
    distance = 	sqrt(mLat*mLat + mLon*mLon); 	//纬度1度 = 大约111km = 111319.5米
    return distance;
}
/*
计算两个点的连线的 航向角， 以正北为0 。
lat1 lon1  点1的经纬度  单位度
lat2 lon2  点2的经纬度
返回 的航向角，单位度。
*/
double getHeading(double lat1,double lon1,double lat2,double lon2){
    double temp;
    double mLat = lat2 - lat1;
    double mLon = (lon2 - lon1)* cos(((lat2 + lat1)/2)* 0.0174532925);
    temp = 90.0 + atan2(-mLat, mLon) * 57.2957795;

    if(temp < 0)temp += 360.0;
    return temp;
}
void MyThread::getCurrentLoc(const QString &lat , const QString &lon)
{
    currentLat  = lat;
    currentLon =lon;
}
void MyThread::getTargetLoc(const QString &lat, const QString &lon)
{
    targetLat = lat;
    targetLon =lon;
}
void MyThread::stop()
{
   stopped_=true;
   sendEvent_=false;
}
void MyThread::startCom()
{
   stopped_=false;

}
void MyThread::changeComState(bool stat)
 {
        com_opened_=stat;
}
void MyThread::setMessage(const QString &message)
{
   messageStr_ = message;
}
void MyThread::setPortnum(const QString &num)
{
    portnum_=num;
}
void MyThread::setPortBaudRate(const QString &BaudRate)
{
    portBautRate_=BaudRate;
}
void MyThread:: changeTxState(bool stat)
{
        sendEvent_=stat;
}
void MyThread:: changeRxState(bool stat)
{
         receiveEvent_=stat;
}
//约束自主导航的条件之一
void MyThread::boat_system_start_n()
{
     sendEvent_=true;
}
void MyThread::boat_system_stop_n()
{
    sendEvent_=false;
}
//约束自主导航的条件之二
void MyThread::boat_mode_P()
{
    boatMode_=false;
}
void MyThread::boat_mode_N()
{
    boatMode_=true;
}

