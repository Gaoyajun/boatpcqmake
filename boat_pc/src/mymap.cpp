#include "include/mymap.h"

Mymap::Mymap()
{
    setWindowTitle("Mymap");

    baiduMap=new QWebView;

    LineEditCurrentLat      = new QLineEdit;
    LineEditCurrentLon     = new QLineEdit;
    LineEditTargetLat         = new QLineEdit;
    LineEditTargetLon        = new QLineEdit;
    LineEditIdentification  =new QLineEdit;

    LaberCurrentLat            = new QLabel ("当前纬度 : ");
    LaberCurrentLon           = new QLabel ("当前经度 :");
    LabelTargetLat               = new QLabel ("目标纬度 :");
    LabelTargetLon              = new QLabel ("目标经度 :");

    pushbuttonSetIdentification  = new QPushButton ("设置标识大小:");
    pushbuttonSendTargetLocal  = new QPushButton ("发送目标地点");
    pushbuttonGetDistance          = new QPushButton ("获取距离");
    pushbuttonQuit                       = new QPushButton("退出");


    QGridLayout *layout = new QGridLayout(this);
    layout->addWidget(LaberCurrentLon,    0,0);
    layout->addWidget(LineEditCurrentLon,0,1,1,2);
    layout->addWidget(LaberCurrentLat,     0,3);
    layout->addWidget(LineEditCurrentLat, 0,4,1,2);

    layout->addWidget(baiduMap,1,0,7,11);

    layout->addWidget(LabelTargetLon,                       8,0);
    layout->addWidget (LineEditTargetLon,                  8,1,1,2);
    layout->addWidget (LabelTargetLat,                        8,3);
    layout->addWidget(LineEditTargetLat,                     8,4,1,2);
    layout->addWidget(pushbuttonSetIdentification,  8,6);
    layout->addWidget(LineEditIdentification,              8,7,1,1);
    layout->addWidget(pushbuttonSendTargetLocal,   8,8);
    layout->addWidget(pushbuttonGetDistance,           8,9);
    layout->addWidget(pushbuttonQuit,                         8,10);

    this->resize(900,600);

    connect(pushbuttonQuit,SIGNAL(clicked()),this,SLOT(close()));
   // connect(pushbuttonSendTargetLocal,SIGNAL(clicked()),this,SLOT(emitSignalSendTarget()));
    connect(pushbuttonSendTargetLocal,SIGNAL(clicked()),this,SLOT(on_map_Button_clicked()));
    connect(pushbuttonSetIdentification,SIGNAL(clicked()),this,SLOT(on_map_button_size_clicked()));
    connect(pushbuttonGetDistance,SIGNAL(clicked()),this,SLOT(on_getDistance_clicked()));

    radiusSize_ = 2;   //默认画圆的半径
    strCity_ = "深圳";  //显示中心点
   // F:\boat_second_stage\pc_boat
    QUrl url("file:///home/gaoyajun/test_git_boat/boat_pc/mymap.html");
    baiduMap->setUrl(url);  //加载网页
}
//设置地图指定中心点
void Mymap::on_webView_loadFinished(bool)
{
    QWebFrame *frame = baiduMap->page()->mainFrame();
    //设置目标城市
    QString method = QString("SetCity(\"%1\")").arg(strCity_);
    frame->evaluateJavaScript(method);
}

void Mymap::on_map_button_size_clicked()
{
    QString strLineText = LineEditIdentification->displayText();
    radiusSize_ = strLineText.toInt();
    QWebFrame *frame = baiduMap->page()->mainFrame();

    QString method = QString("SetRadius(\"%1\")").arg(radiusSize_);
    frame->evaluateJavaScript(method);
    if(radiusSize_ != 2)
    {
        frame->evaluateJavaScript("AutoSetCircle()");
    }
}

void Mymap::on_map_Button_clicked()
{
    double lat=0.0,lon=0.0;
    QWebFrame *frame = baiduMap->page()->mainFrame();
    QVariant var = frame->evaluateJavaScript("ReturnLngLat()");
    QStringList strList = var.toStringList();
    //将返回的值作为终点的坐标
    QString c_lon=strList.at(0);
    QString c_lat=strList.at(1);
    LineEditTargetLon->setText(c_lon);
    LineEditTargetLat->setText(c_lat);
    qDebug()<<"c_lon="<<c_lon<<"    c_lat="<<c_lat<<endl;

    QString qs_lon=QString("get_lon(\"%1\")").arg(lon,0,'g',9);
    frame->evaluateJavaScript(qs_lon);

    QString qs_lat=QString("get_lat(\"%1\")").arg(lat,0,'g',8);
    frame->evaluateJavaScript(qs_lat);
    frame->evaluateJavaScript("add_marker()");

    for(int i=0; i<strList.count(); i++)
    {
        QMessageBox::information(NULL, QObject::tr("Lng&Lat"), strList.at(i));
    }
    emit(sendTargetLoc(c_lat,c_lon));
}

void Mymap::updateLocal()
{
    double c_lon,c_lat,s_lat,s_lon;
    QWebFrame *frame = baiduMap->page()->mainFrame();

    QString s_lat2=LineEditCurrentLat->displayText();
    QString s_long2=LineEditCurrentLon->displayText();
    QString c_lon2=LineEditTargetLon->displayText();
    QString c_lat2=LineEditTargetLat->displayText();
    c_lon=c_lon2.toDouble();
    c_lat=c_lat2.toDouble();
    s_lat=s_lat2.toDouble();
    s_lon=s_long2.toDouble();

    //确定目前船的位置
    QString qs_lon=QString("get_lon(\"%1\")").arg(s_lon,0,'g',9);
    frame->evaluateJavaScript(qs_lon);
    QString qs_lat=QString("get_lat(\"%1\")").arg(s_lat,0,'g',8);
    frame->evaluateJavaScript(qs_lat);

    //设置终点
    QString qc_lon=QString("set_lon(\"%1\")").arg(c_lon,0,'g',9);
    frame->evaluateJavaScript(qc_lon);
    QString qc_lat=QString("set_lat(\"%1\")").arg(c_lat,0,'g',8);
    frame->evaluateJavaScript(qc_lat);
    frame->evaluateJavaScript("add_marker()");
}

void Mymap::on_getDistance_clicked()
{
    QWebFrame *frame = baiduMap->page()->mainFrame();
    frame->evaluateJavaScript("ABdistance()");
}

void Mymap::emitSignalSendTarget()
{
    QString targetLat  = LineEditTargetLat->text();
    QString targetLon = LineEditTargetLon->text();
    emit sendTargetLoc(targetLat , targetLon);
}

void Mymap::getCurrentLoc(const QString &lat, const QString &lon)
{
    QString currentLat = lat;
    QString currentLon =lon;
    LineEditCurrentLat->setText(currentLat);
    LineEditCurrentLon->setText(currentLon);
}

void Mymap::getBoatState(bool state)
{
    if(state==true)
    {
        pushbuttonSendTargetLocal->setEnabled(true);
        pushbuttonGetDistance->setEnabled(true);
    }
    else{
        pushbuttonSendTargetLocal->setEnabled(false);
        pushbuttonGetDistance->setEnabled(false);
    }
}
