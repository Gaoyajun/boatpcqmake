#include "include/mywaterinf.h"

MyWaterInf::MyWaterInf()
{
    setWindowTitle("MyWaterInformation");

    laberWaterInf     = new QLabel("水温信息：");
    lineEditWaterInf = new QLineEdit("******");
    pushbuttonQuit  =new QPushButton("Quit");

    QGridLayout     *layout   = new QGridLayout(this);
    layout->addWidget(laberWaterInf,0,0);
    layout->addWidget(lineEditWaterInf,0,1);
    layout->addWidget(pushbuttonQuit,1,1);

    this->resize(300,200);

    connect(pushbuttonQuit,SIGNAL(clicked()),this,SLOT(close()));

}

void MyWaterInf::getWaterInf(const QString &str)
{
    lineEditWaterInf->setText(str);
}
