This ia an automatic boat project originated from a course project (Course name: design of modern control system), maintained by master students Yajun Gao and Ting Li.

What is this repository for?
A PC  program to achieve manual control and automatic navigation of the ship, and can see motion information and system information.

How to implement these functions?

Requirement：Qt5 and above
	     ubuntu/windows Operating system

Step1: Communication setup
  Select the serial number and baud rate
  If there is no serial number, it may be that you do not read and write permissions USB port, so you need to do the following work:
    $ ls /dev         //Check your device port number
    method 1: $ sudo chmod 666 yourserialport   //Temporarily change your permissions
    method 2: $ sudo usermod -aG dialout teashaw      //Permanently change your permissions

  Than open then serial port

step 2: Open the system
  Before opening the system, you need to select the mode.If the boat sends data to you, you can see the current position of the boat, the boat's direction, the water temperature information (by clicking the water temperature information button),and you can check the map. If no map is displayed, it may be the path of the HTML file is not correct, so  you can change the path of the HTML file in the program.
1）Remote control mode
  You can control the direction and speed of the ship.
2) Autonomous navigation mode
  If you choose the autonomous navigation mode, you can set the target position on any one point on the map, and can obtain the distance between the current ship's position and the target position.

Step 3: Exit
